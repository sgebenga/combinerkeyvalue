# README #

### What is this repository for? ###

* A Hadoop application with a custom combiner class, custom key value and output value class 
* Version 0.1

### How do I get set up? ###

* $ mvn clean install; hadoop jar target/combiner-key-value-1.0-SNAPSHOT.jar combiner.ExpenseCategoryApp data/in data/out
* Dependencies : Requires Hadoop 2.* and Maven

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact