package combiner;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ExpenseCategoryKey implements WritableComparable<ExpenseCategoryKey> {


    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    LocalDate time;//= LocalDateTime.parse(now, formatter);

    private String date;
    private String expense;

    public int compareTo(ExpenseCategoryKey o) {
        int result = time.compareTo(o.time);
        if (result == 0) {
            result = expense.compareTo(o.expense);
        }
        return result;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(expense);
        dataOutput.writeInt(time.getYear());
        dataOutput.writeInt(time.getMonthValue());
        dataOutput.writeInt(time.getDayOfMonth());
    }

    public void readFields(DataInput dataInput) throws IOException {
        expense = dataInput.readUTF();
        time = LocalDate.of(dataInput.readInt(), dataInput.readInt(), dataInput.readInt());

    }

    public void setTime(String t) {
        this.time = LocalDate.parse(t, formatter);
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String toString() {
        return formatter.format(time) + "," + expense;
    }

    public LocalDate getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public String getExpense() {
        return expense;
    }
}
