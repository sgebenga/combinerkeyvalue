package combiner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class ExpenseCategoryApp extends Configured implements Tool {

    public static void main(String[] args) throws Exception {
        int result = ToolRunner.run(new Configuration(), new ExpenseCategoryApp(), args);
        System.exit(result);
    }

    public int run(String[] args) throws Exception {
        Configuration configuration = getConf();
        configuration.set("mapreduce.output.textoutputformat.separator", ",");

        Job job = Job.getInstance(configuration, "custom value with combiner app");
        job.setJarByClass(ExpenseCategoryApp.class);

        Path in = new Path(args[0]);
        Path out = new Path(args[1]);

        FileInputFormat.addInputPath(job, in);
        FileOutputFormat.setOutputPath(job, out);

        job.setMapperClass(ExpenseCategoryMapper.class);
        job.setMapOutputKeyClass(ExpenseCategoryKey.class);
        job.setMapOutputValueClass(ExpenseCategoryValue.class);

        job.setReducerClass(ExpenseCategoryReducer.class);
        job.setOutputKeyClass(ExpenseCategoryKey.class);
        job.setOutputValueClass(ExpenseCategoryValue.class);

      //  job.setCombinerClass(ExpenseCategoryCombiner.class);

        job.setPartitionerClass(ExpenseCategoryPartitioner.class);
        job.setNumReduceTasks(12);

        FileSystem fs = FileSystem.get(configuration);

        if (fs.exists(out)) {
            fs.delete(out, true);
        }

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
