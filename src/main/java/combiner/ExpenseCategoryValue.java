package combiner;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ExpenseCategoryValue implements WritableComparable<ExpenseCategoryValue> {

    private Double value;
    private Double acum = 0.0d;
    private Long count = 0L;

    public ExpenseCategoryValue() {
    }

    public ExpenseCategoryValue(Double value) {
        this.value = value;
    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeDouble(value);
        dataOutput.writeDouble(acum);
        dataOutput.writeLong(count);
    }

    public void readFields(DataInput dataInput) throws IOException {
        value = dataInput.readDouble();
        acum = dataInput.readDouble();
        count = dataInput.readLong();
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getAcum() {
        return acum;
    }

    public void setAcum(double acum) {
        this.acum = acum;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    @Override
    public int compareTo(ExpenseCategoryValue o) {
        int result = value.compareTo(o.value);
        if (result == 0) {
            result = acum.compareTo(o.acum);
        }
        if (result == 0) {
            result = count.compareTo(o.count);
        }
        return result;
    }

    @Override
    public String toString() {
        return (value != null) ? value.toString() : "0.00";
    }
}
