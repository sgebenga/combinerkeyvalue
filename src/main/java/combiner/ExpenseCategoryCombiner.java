package combiner;

import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ExpenseCategoryCombiner extends Reducer<ExpenseCategoryKey, ExpenseCategoryValue, ExpenseCategoryKey, ExpenseCategoryValue> {

    @Override
    protected void reduce(ExpenseCategoryKey key, Iterable<ExpenseCategoryValue> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        long count = 0;
        for (ExpenseCategoryValue value : values) {
            acum += value.getValue();
            count++;
        }
        ExpenseCategoryValue value = new ExpenseCategoryValue(acum);
        //value.setAcum(acum);
        //value.setCount(count);
        context.write(key, value);
    }
}
