package combiner;

import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ExpenseCategoryReducer extends Reducer<ExpenseCategoryKey, ExpenseCategoryValue, ExpenseCategoryKey, ExpenseCategoryValue> {
    @Override
    protected void reduce(ExpenseCategoryKey key, Iterable<ExpenseCategoryValue> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        for (ExpenseCategoryValue value : values) {
            acum += value.getValue();
        }
        ExpenseCategoryValue value = new ExpenseCategoryValue(acum);
        context.write(key, value);
    }
}
