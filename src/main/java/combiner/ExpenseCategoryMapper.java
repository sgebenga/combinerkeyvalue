package combiner;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class ExpenseCategoryMapper extends Mapper<LongWritable, Text, ExpenseCategoryKey, ExpenseCategoryValue> {

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] csv = value.toString().split(",");
        ExpenseCategoryKey k = new ExpenseCategoryKey();

        try {
            k.setTime(csv[0]);
            k.setExpense(csv[1]);
            context.write(k, new ExpenseCategoryValue(new Double(csv[2])));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
